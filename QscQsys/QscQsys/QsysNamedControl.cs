﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Crestron.SimplSharp;
using Newtonsoft.Json;

namespace QscQsys
{
    public class QsysNamedControl
    {
        private readonly string _name;

        private bool _boolValue;
        private int _integerValue;
        private List<string> _listValue; 
        private string _stringValue;

        public bool BoolValue
        {
            get { return _boolValue; }
            set
            {
                if (_boolValue == value) return;
                _boolValue = value;

                ControlSetInteger setControl = new ControlSetInteger();
                setControl.@params = new ControlSetIntegerParams() { Name = _name, Value = _boolValue ? 1 : 0};
                QsysCore.Enqueue(JsonConvert.SerializeObject(setControl));
            }
        }

        public int IntegerValue
        {
            get { return _integerValue; }
            set
            {
                if (_integerValue == value) return;
                _integerValue = value;

                ControlSetInteger setControl = new ControlSetInteger();
                setControl.@params = new ControlSetIntegerParams() {Name = _name, Value = _integerValue};
                QsysCore.Enqueue(JsonConvert.SerializeObject(setControl));
            }
        }

        public List<string> ListValue
        {
            get { return _listValue; }
            set { _listValue = value; }
        }

        public string StringValue
        {
            get { return _stringValue; }
            set
            {
                if (_stringValue == value) return;
                _stringValue = value;

                ControlSetString setControl = new ControlSetString();
                setControl.@params = new ControlSetStringParams() {Name = _name, Value = _stringValue};
                QsysCore.Enqueue(JsonConvert.SerializeObject(setControl));
            }
        }

        public event EventHandler<QsysEventArgs> OnControlChangeEvent;

        public QsysNamedControl(string controlName)
        {
            _name = controlName;

            Control control = new Control();
            control.Name = _name;

            if (QsysCore.RegisterControl(control))
            {
                QsysCore.Controls[control].OnNewEvent += new EventHandler<InternalEventArgs>(QsysNamedControl_OnNewEvent);
            }

        }

        void QsysNamedControl_OnNewEvent(object sender, InternalEventArgs e)
        {
            _boolValue = (e.Value == 1);
            _integerValue = (int)e.Value;
            _listValue = e.Choices;
            _stringValue = e.StringValue;
            OnControlChangeEvent(this, new QsysEventArgs(_name, _boolValue, _integerValue, e.Position, _stringValue, null));
        }

    }
}