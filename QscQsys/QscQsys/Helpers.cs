﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Crestron.SimplSharp;

namespace QscQsys
{
    public static class Helpers
    {
        public static string FormatPhoneNumber(string number)
        {
            if (number == null)
                return "";

            Match match = Regex.Match(number, @"\D");
            if (match.Success)
            {
                return number;
            }

            if (number.Length == 7)
                return Convert.ToInt64(number).ToString("###-####");

            if (number.Length == 8)
                return Convert.ToInt64(number).ToString("#-###-####");

            if (number.Length == 9)
                return Convert.ToInt64(number).ToString("#-#-###-####");

            if (number.Length == 10)
                return Convert.ToInt64(number).ToString("(###)###-####");

            if (number.Length == 11)
                return Convert.ToInt64(number).ToString("#(###)###-####");

            if (number.Length == 12)
                return Convert.ToInt64(number).ToString("#-#(###)###-####");

            return number;
        }

        public static string FormatHtmlTextColor(int textSize, string stringValue, string hexColor)
        {
            return String.Format("<FONT size=\"{0}\" color=\"{1}\">{2}</FONT>", textSize, hexColor, stringValue);
        }

        public static ushort LogScaleReturnValue(double dbValue)
        {
            double positionValue = Math.Pow(10, (dbValue / 20));

            if (positionValue <= 0.00001) positionValue = 0;

            return Convert.ToUInt16(positionValue * 100f * 655.35);
        }

        public static double LogSetFaderValue(ushort faderValue)
        {
            double newValue = (double)faderValue / 65535.00;
            if (newValue <= 0.00001) newValue = 0.00001;
            return Math.Log10(newValue) * 20;
        }
    }
}