﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using Crestron.SimplSharp;
using Newtonsoft.Json;

namespace QscQsys
{
    public class QsysGainComponent
    {
        private readonly string _name;
        public readonly bool IsRegistered;
        public bool MuteState { get; private set; }
        public double IntegerLevel { get; private set; }
        public double Position { get; private set; }

        public bool Debug;

        public event EventHandler<QsysEventArgs> OnComponentChangeEvent;

        public QsysGainComponent(string componentName)
        {
            _name = componentName;

            Component component = new Component();
            component.Name = componentName;
            component.Controls = new List<ControlName>();
            component.Controls.Add(new ControlName());
            component.Controls.Add(new ControlName());
            component.Controls[0].Name = "gain";
            component.Controls[1].Name = "mute";

            if (QsysCore.RegisterComponent(component))
            {
                QsysCore.Components[component].OnNewEvent += new EventHandler<InternalEventArgs>(QsysGainComponent_OnNewEvent);
                IsRegistered = true;
            }
        }

        void QsysGainComponent_OnNewEvent(object sender, InternalEventArgs e)
        {
            switch (e.Name)
            {
                case "gain":
                {
                    IntegerLevel = e.Value;
                    Position = e.Position;
                    OnComponentChangeEvent(this, new QsysEventArgs(_name, e.Name, false, IntegerLevel, Position, IntegerLevel.ToString(), null));
                    if(Debug) CrestronConsole.PrintLine("QsysGainComponent {0} gain event: {1}", _name, IntegerLevel);
                    break;
                }
                case "mute":
                {
                    MuteState = (e.Value == 1);
                    OnComponentChangeEvent(this, new QsysEventArgs(_name, e.Name, MuteState, Convert.ToDouble(MuteState), e.Position, MuteState.ToString(), null));
                    break;
                }

            }
        }

        public void SetPosition(ushort value)
        {
            double position = value/65535.00;
            if (position != value)
            {
                ComponentSetInteger componentChange = new ComponentSetInteger();

                componentChange.@params = new ComponentSetIntegerParams {Name = _name};

                ComponentSetIntegerValue volume = new ComponentSetIntegerValue
                {
                    Name = "gain",
                    Position = position
                };

                componentChange.@params.Controls = new List<ComponentSetIntegerValue> {volume};

                QsysCore.Enqueue(JsonConvert.SerializeObject(componentChange, Formatting.None, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore }));
 
            }
        }

        public void SetValue(double value)
        {
            if (IntegerLevel != value)
            {
                ComponentSetInteger componentChange = new ComponentSetInteger();

                componentChange.@params = new ComponentSetIntegerParams { Name = _name };

                ComponentSetIntegerValue volume = new ComponentSetIntegerValue
                {
                    Name = "gain",
                    Value = value
                };

                componentChange.@params.Controls = new List<ComponentSetIntegerValue> { volume };

                QsysCore.Enqueue(JsonConvert.SerializeObject(componentChange, Formatting.None, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore }));

            }
        }

        public void Mute(bool value)
        {
            if (MuteState != value)
            {
                ComponentSetInteger componentChange = new ComponentSetInteger();

                componentChange.@params = new ComponentSetIntegerParams {Name = _name};

                ComponentSetIntegerValue mute = new ComponentSetIntegerValue {Name = "mute", Value = value ? 1 : 0};

                componentChange.@params.Controls = new List<ComponentSetIntegerValue> { mute };

                QsysCore.Enqueue(JsonConvert.SerializeObject(componentChange, Formatting.None, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore}));

            }
        }
    }
}