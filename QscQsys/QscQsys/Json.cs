﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Crestron.SimplSharp;
using Newtonsoft.Json;

namespace QscQsys
{
    public class Component
    {
        public string Name { get; set; }
        public IList<ControlName> Controls { get; set; }
    }

    public class Control
    {
        public string Name { get; set; }
    }

    public class ControlName
    {
        public string Name { get; set; }
    }

    public class QsysMethodBase
    {
        [JsonProperty]
        private static string jsonrpc = "2.0";
    }

    #region Change Group classes
    public class AddComponentToChangeGroup : QsysMethodBase
    {
        [JsonProperty]
        private static string id = "crestron";
        [JsonProperty]
        private static string method = "ChangeGroup.AddComponentControl";

        [JsonProperty("params")]
        public AddComponentToChangeGroupParams ComponentParams { get; set; }
    }

    public class AddComponentToChangeGroupParams
    {
        [JsonProperty]
        private static string Id = "1";
        public Component Component { get; set;  }
    }

    public class AddControlToChangeGroup : QsysMethodBase
    {
        [JsonProperty]
        private static string id = "crestron";

        [JsonProperty]
        private static string method = "ChangeGroup.AddControl";

        [JsonProperty("params")]
        public AddControlToChangeGroupParams ControlParams { get; set; }

    }

    public class AddControlToChangeGroupParams
    {
        [JsonProperty]
        static string Id = "1";

        public List<string> Controls { get; set; }
    }

    public class CreateChangeGroup : QsysMethodBase
    {
        [JsonProperty]
        private static string id = "crestron";

        [JsonProperty]
        private static string method = "ChangeGroup.AutoPoll";

        [JsonProperty("params")]
        public CreateChangeGroupParams @params = new CreateChangeGroupParams();
    }

    public class CreateChangeGroupParams
    {
        [JsonProperty]
        private static string Id = "1";

        [JsonProperty]
        private static double Rate = 0.15;
    }

    public class ChangeResult
    {
        [JsonProperty(Required = Required.Default)]
        public string Component { get; set; }

        public string Name { get; set; }
        public string String { get; set; }
        public double Value { get; set; }
        public double Position { get; set; }
        public IList<string> Choices { get; set; }
        public string Legend { get; set; }
    }
    #endregion

    public class Heartbeat: QsysMethodBase
    {
        [JsonProperty]
        public static string method = "NoOp";

        [JsonProperty("params")]
        object @params = new object();
    }

    #region Component classes

    public class ComponentSetInteger : QsysMethodBase
    {
        [JsonProperty]
        private static string id = "crestron";

        [JsonProperty]
        private static string method = "Component.Set";

        [JsonProperty("params")]
        public ComponentSetIntegerParams @params { get; set; }
    }

    public class ComponentSetIntegerParams
    {
        public string Name { get; set; }
        public IList<ComponentSetIntegerValue> Controls { get; set; }
    }

    public class ComponentSetIntegerValue
    {
        public string Name { get; set; }

        [JsonProperty(Required = Required.Default)]
        public double? Value { get; set; } 

        [JsonProperty(Required = Required.Default)]
        public double? Position { get; set; }

        public ComponentSetIntegerValue()
        {
            this.Value = null;
            this.Position = null;
        }
    }

    public class ComponentSetString : QsysMethodBase
    {
        [JsonProperty]
        private static string id = "crestron";

        [JsonProperty]
        private static string method = "Component.Set";

        [JsonProperty("params")]
        public ComponentSetStringParams @params { get; set; }
    }

    public class ComponentSetStringParams
    {
        public string Name { get; set; }
        public IList<ComponentSetStringValue> Controls { get; set; }
    }

    public class ComponentSetStringValue
    {
        public string Name { get; set; }
        public string Value { get; set; }
    }

    #endregion

    #region Control classes

    public class ControlSetInteger : QsysMethodBase
    {
        [JsonProperty]
        private static string id = "crestron";

        [JsonProperty]
        private static string method = "Control.Set";

        [JsonProperty("params")]
        public ControlSetIntegerParams @params { get; set; }
    }

    public class ControlSetIntegerParams
    {
        public string Name { get; set; }

        [JsonProperty(Required = Required.Default)]
        public double? Value { get; set; }

        [JsonProperty(Required = Required.Default)]
        public double? Position { get; set; }

        public ControlSetIntegerParams()
        {
            this.Value = null;
            this.Position = null;
        }
    }

    public class ControlSetString : QsysMethodBase
    {
        [JsonProperty]
        private static string id = "crestron";

        [JsonProperty]
        private static string method = "Control.Set";

        [JsonProperty("params")]
        public ControlSetStringParams @params { get; set; }
    }

    public class ControlSetStringParams
    {
        public string Name { get; set; }
        public string Value { get; set; }
    }

    #endregion

    public class EngineStatus
    {
        public string State { get; set; }
        public string DesignCode { get; set; }
        public string DesignName { get; set; }
        public bool IsRedundant { get; set; }
        public bool IsEmulator { get; set; }
    }

    public class ListBoxChoice
    {
        public string Text { get; set; }
        public string Color { get; set; }
        public string Icon { get; set; }
    }

    public class Legend
    {
        public bool DrawChrome { get; set; }
        public string IconData { get; set; }
    }

}