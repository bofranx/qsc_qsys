﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Crestron.SimplSharp;
using Newtonsoft.Json;

namespace QscQsys
{
    public class QsysCameraComponent
    {
        private readonly string _name;
        public readonly bool IsRegistered;
        public string Jpeg { get; private set; }
        public string PtzPreset { get; private set; }
        public string UrlStream1 { get; private set; }
        public string UrlStream2 { get; private set; }

        public bool Debug { get; set; }

        public event EventHandler<QsysEventArgs> OnComponentChangeEvent;

        public QsysCameraComponent(string componentName)
        {
            _name = componentName;

            Component component = new Component();
            component.Name = componentName;
            component.Controls = new List<ControlName>
            {
                new ControlName() { Name = "ptz.preset" },
                new ControlName() { Name = "url.stream.1.display" },
                new ControlName() { Name = "url.stream.2.display" }
            };


            if (QsysCore.RegisterComponent(component))
            {
                QsysCore.Components[component].OnNewEvent += new EventHandler<InternalEventArgs>(QsysCameraComponent_OnNewEvent);
                IsRegistered = true;
            }
        }

        void QsysCameraComponent_OnNewEvent(object sender, InternalEventArgs e)
        {
            if (Debug)
                CrestronConsole.PrintLine("QsysCameraComponent_OnNewEvent({0}):\r{1} - {2}", _name, e.Name, e.StringValue);

            switch (e.Name)
            {
                case "ptz.preset":
                    PtzPreset = e.StringValue;
                    break;

                case "url.stream.1.display":
                    UrlStream1 = e.StringValue;
                    break;

                case "url.stream.2.display":
                    UrlStream2 = e.StringValue;
                    break;
            }
            
        }

        public void PtzControl(ePtzType ptzType, bool onOff)
        {
            ComponentSetInteger setComponent = new ComponentSetInteger();
            setComponent.@params = new ComponentSetIntegerParams();
            setComponent.@params.Name = _name;
            
            ComponentSetIntegerValue value = new ComponentSetIntegerValue();

            switch (ptzType)
            {
                case ePtzType.Up:
                    value.Name = "tilt.up";
                    break;
                case ePtzType.Down:
                    value.Name = "tilt.down";
                    break;
                case ePtzType.Left:
                    value.Name = "pan.left";
                    break;
                case ePtzType.Right:
                    value.Name = "pan.right";
                    break;
                case ePtzType.ZoomIn:
                    value.Name = "zoom.in";
                    break;
                case ePtzType.ZoomOut:
                    value.Name = "zoom.out";
                    break;
            }

            value.Value = Convert.ToDouble(onOff);
            setComponent.@params.Controls.Add(value);
            QsysCore.Enqueue(JsonConvert.SerializeObject(setComponent));
        }

        public enum ePtzType
        {
            Up = 1,
            Down = 2,
            Left = 3,
            Right = 4,
            ZoomIn = 5,
            ZoomOut = 6
        }
    }
}