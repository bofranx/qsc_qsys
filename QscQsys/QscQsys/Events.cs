﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Crestron.SimplSharp;

namespace QscQsys
{
    public class QsysEventArgs : EventArgs
    {
        public string ComponentName;
        public string ControlName;
        public bool BooleanValue;
        public double IntegerValue;
        public double PositionValue;
        public string StringValue;
        public List<ListBoxChoice> ListValue;

        public QsysEventArgs(string controlName, bool booleanValue, double integerValue, double positionValue, string stringValue, List<ListBoxChoice> listValue)
        {
            this.ComponentName = null;
            this.ControlName = controlName;
            this.BooleanValue = booleanValue;
            this.IntegerValue = integerValue;
            this.PositionValue = positionValue;
            this.StringValue = stringValue;
            this.ListValue = listValue;
        }

        public QsysEventArgs(string componentName, string controlName, bool booleanValue, double integerValue, double positionValue, string stringValue, List<ListBoxChoice> listValue)
        {
            this.ComponentName = componentName;
            this.ControlName = controlName;
            this.BooleanValue = booleanValue;
            this.IntegerValue = integerValue;
            this.PositionValue = positionValue;
            this.StringValue = stringValue;
            this.ListValue = listValue;
        }
    }

    public class CoreStatusEventArgs : EventArgs
    {
        public string State;
        public string DesignCode;
        public string DesignName;
        public bool IsRedundant;
        public bool IsEmulator;

        public CoreStatusEventArgs(string state, string designCode, string designName, bool isRedundant, bool isEmulator)
        {
            this.State = state;
            this.DesignCode = designCode;
            this.DesignName = designName;
            this.IsRedundant = isRedundant;
            this.IsEmulator = isEmulator;
        }
    }

    internal class InternalEventArgs : EventArgs
    {
        public string Name;
        public double Value;
        public double Position;
        public string StringValue;
        public List<string> Choices;

        public InternalEventArgs(string name, double intValue, double position, string stringValue, List<string> choices)
        {
            this.Name = name;
            this.Value = intValue;
            this.Position = position;
            this.StringValue = stringValue;
            this.Choices = choices;
        }
    }

    internal class InternalEvents
    {
        public event EventHandler<InternalEventArgs> OnNewEvent;
        //private event EventHandler<InternalEventArgs> onNewEvent = delegate { };
        //public event EventHandler<InternalEventArgs> OnNewEvent
        //{
        //    add
        //    {
        //        if (!onNewEvent.GetInvocationList().Contains(value))
        //        {
        //            onNewEvent += value;
        //        }
        //    }
        //    remove { if (onNewEvent != null) onNewEvent -= value; }
        //}

        internal void FireEvent(InternalEventArgs e)
        {
            OnNewEvent(null, e);
        }
    }
}