﻿using System;
using System.Linq;
using Crestron.SimplSharp;                          				// For Basic SIMPL# Classes
using Crestron.SimplSharp.CrestronSockets;
using Crestron.SimplSharpPro;
using System.Collections.Generic; // For Basic SIMPL#Pro classes
using Newtonsoft.Json;
using TCPLibrary;
using System.Text;
using Newtonsoft.Json.Linq;

namespace QscQsys
{
    public static class QsysCore
    {
        public static event EventHandler<CoreStatusEventArgs> CoreStatusEvent;

        private static CrestronQueue<string> commandQueue;
        private static CrestronQueue<string> responseQueue;
        private static CTimer commandQueueTimer;
        private static CTimer responseQueueTimer;
        private static CTimer heartbeatTimer;
        public static TCPClientDevice client;

        public static bool Initialized { get; private set; }
        public static bool Connected { get; private set; }

        private static bool _debug;
        public static bool Debug
        {
            get { return _debug; }
            set
            {
                _debug = value;
                if(client != null)
                    client.Debug = value;
            }
        }

        public static string DesignName { get; private set; }
        public static bool IsEmulator { get; private set; }
        public static bool IsRedundant { get; private set; }

        internal static Dictionary<Component, InternalEvents> Components = new Dictionary<Component,InternalEvents>();
        internal static Dictionary<Control, InternalEvents> Controls = new Dictionary<Control, InternalEvents>();

        public static void Dispose()
        {
            if (Initialized)
            {
                client.Dispose();
                
                commandQueueTimer.Stop();
                commandQueueTimer.Dispose();
                commandQueue.Dispose();
               
                responseQueueTimer.Stop();
                responseQueueTimer.Dispose();
                responseQueue.Dispose();

                if (!heartbeatTimer.Disposed)
                {
                    heartbeatTimer.Stop();
                    heartbeatTimer.Dispose();
                }
            }
        }

        public static void Enqueue(string data)
        {
            commandQueue.Enqueue(data);
        }

        public static void Initialize(string host, ushort port)
        {
            if (!Initialized)
            {
                CrestronConsole.PrintLine("QsysCore @ {0} initializing", host);
                commandQueue = new CrestronQueue<string>();
                commandQueueTimer = new CTimer(CommandQueueProcessor, null, 0, 50);
                                
                responseQueue = new CrestronQueue<string>();
                responseQueueTimer = new CTimer(ResponseQueueProcessor, null, 0, 50);

                client = new TCPClientDevice(host, 1710);
                client.Debug = false;
                client.Description = "QSC Q-SYS";
                client.ConnectionStatus += new TCPClientDevice.ConnectionStatusHandler(client_ConnectionStatus);
                client.ResponseString += new TCPClientDevice.ResponseEventHandler(client_ResponseString);
                client.ConnectToServer();
            }
        }

        static void client_ConnectionStatus(SocketStatus status)
        {
            if (status == SocketStatus.SOCKET_STATUS_CONNECTED)
            {
                CrestronConsole.PrintLine("QsysCore @ {0} is connected", client.AddressClientConnectedTo);
                Connected = true;

                AddComponentToChangeGroup addComponent;

                foreach (var item in Components)
                {
                    addComponent = new AddComponentToChangeGroup();
                    addComponent.ComponentParams = new AddComponentToChangeGroupParams();
                    addComponent.ComponentParams.Component = item.Key;
                    commandQueue.Enqueue(JsonConvert.SerializeObject(addComponent));
                }


                AddControlToChangeGroup addControl = new AddControlToChangeGroup();
                foreach (var item in Controls)
                {
                    
                    addControl.ControlParams = new AddControlToChangeGroupParams();
                    addControl.ControlParams.Controls = new List<string>();
                    addControl.ControlParams.Controls.Add(item.Key.Name);
                    commandQueue.Enqueue(JsonConvert.SerializeObject(addControl));
                }

                
                commandQueue.Enqueue(JsonConvert.SerializeObject(new CreateChangeGroup()));

                heartbeatTimer = new CTimer(SendHeartbeat, null, 0, 15000);

                CrestronConsole.PrintLine("QsysCore @ {0} is initialized", client.AddressClientConnectedTo);
                Initialized = true;
            }
            else if (Connected && status != SocketStatus.SOCKET_STATUS_CONNECTED)
            {
                CrestronConsole.PrintLine("QsysCore @ {0} disconnected", client.AddressClientConnectedTo);

                Connected = false;
                Initialized = false;
                heartbeatTimer.Dispose();
            }
        }

        private static void SendHeartbeat(object o)
        {
            commandQueue.Enqueue(JsonConvert.SerializeObject(new Heartbeat()));
        }

        static void client_ResponseString(string data)
        {
            ParseResponse(data);
        }

        internal static void ParseResponse(string data)
        {
            try
            {
                responseQueue.Enqueue(data);
            }
            catch (Exception e)
            {
                CrestronConsole.PrintLine("QsysCore @ {0} ParseResponse exception: {1}", client.AddressClientConnectedTo, e.Message);
                throw;
            }
        }

        private static void ParseInternalResponse(string returnString)
        {
            if (returnString.Length > 0)
            {
                try
                {
                    JObject response = JObject.Parse(returnString);

                    if (returnString.Contains("Changes"))
                    {
                        IList<JToken> changes = response["params"]["Changes"].Children().ToList();

                        IList<ChangeResult> changeResults = new List<ChangeResult>();

                        foreach (JToken change in changes)
                        {
                            ChangeResult changeResult = JsonConvert.DeserializeObject<ChangeResult>(change.ToString(), new JsonSerializerSettings { MissingMemberHandling = MissingMemberHandling.Ignore });

                            if (changeResult.Component != null)
                            {
                                foreach (var item in Components)
                                {
                                    List<string> choices = changeResult.Choices != null ? changeResult.Choices.ToList() : new List<string>();

                                    if (item.Key.Name == changeResult.Component)
                                        item.Value.FireEvent(new InternalEventArgs(changeResult.Name, changeResult.Value, changeResult.Position, changeResult.String, choices));
                                }
                            }
                            else if (changeResult.Name != null)
                            {
                                List<string> choices = changeResult.Choices != null ? changeResult.Choices.ToList() : new List<string>();

                                foreach (var item in Controls)
                                {
                                    if (item.Key.Name.Contains(changeResult.Name))
                                        item.Value.FireEvent(new InternalEventArgs(changeResult.Name, changeResult.Value, changeResult.Position, changeResult.String, choices));
                                }
                            }
                        }
                    }
                    else if (returnString.Contains("EngineStatus"))
                    {
                        if (response["params"] != null)
                        {
                            EngineStatus status = JsonConvert.DeserializeObject<EngineStatus>(response["params"].ToString());

                            CoreStatusEvent(null, new CoreStatusEventArgs(status.State, status.DesignCode, status.DesignName, status.IsRedundant, status.IsEmulator));
                            CrestronConsole.PrintLine("***QsysCore @ {0} Engine Status***\r\nState: {1} | Design Code: {2} | Design Name: {3} | Is Redudant:{4} | Is Emulator: {5}", 
                                client.AddressClientConnectedTo, status.State, status.DesignCode, status.DesignName, status.IsRedundant, status.IsEmulator);
                        }
                    }
                }
                catch (Exception e)
                {
                    if (Debug)
                        ErrorLog.Error("QsysCore @ {0} ParseInternalResponse exception: {1}:\r\n{2}", client.AddressClientConnectedTo, e.Message, returnString);
                }
            }
        }

        private static void CommandQueueProcessor(object o)
        {
            if (!commandQueue.IsEmpty)
            {
                var data = commandQueue.Dequeue();

                client.SendData(string.Format("{0}\x00", data));
            }
        }

        static StringBuilder RxData = new StringBuilder();
        static bool busy = false;
        static int Pos = -1;
        private static void ResponseQueueProcessor(object o)
        {
            if (!responseQueue.IsEmpty)
            {
                try
                {
                    // removes string from queue, blocks until an item is queued
                    string tmpString = responseQueue.Dequeue();

                    RxData.Append(tmpString); //Append received data to the COM buffer

                    if (!busy)
                    {
                        busy = true;
                        while (RxData.ToString().Contains("\x00"))
                        {
                            Pos = RxData.ToString().IndexOf("\x00");
                            var data = RxData.ToString().Substring(0, Pos);
                            var garbage = RxData.Remove(0, Pos + 1); // remove data from COM buffer

                            if (Debug)
                                CrestronConsole.PrintLine("QsysCore ResponseQueueProcessor response found: {0}", data);

                            ParseInternalResponse(data);
                        }

                        busy = false;
                    }
                }
                catch (Exception e)
                {
                    busy = false;
                    if (Debug)
                        ErrorLog.Error("QsysCore ResponseQueueProcessor exception: {0}", e.Message);
                }

                //ParseInternalResponse(responseQueue.Dequeue());
            }
        }

        

        static internal bool RegisterComponent(Component component)
        {
            try
            {
                lock (Components)
                {
                    if (!Components.ContainsKey(component))
                    {
                        Components.Add(component, new InternalEvents());

                        if (Initialized && Connected)
                        {
                            AddComponentToChangeGroup addComponent = new AddComponentToChangeGroup();
                            addComponent.ComponentParams = new AddComponentToChangeGroupParams();
                            addComponent.ComponentParams.Component = component;

                            commandQueue.Enqueue(JsonConvert.SerializeObject(addComponent));

                            if(Debug)
                                CrestronConsole.PrintLine("QsysCore RegisterComponent {0} registered", component.Name);
                        }
                    }
                }

                return true;
            }
            catch (Exception e)
            {
                CrestronConsole.PrintLine("QsysCore RegisterComponent exception: " + e.Message);
                return false;
            }
        }

        static internal bool RegisterControl(Control control)
        {
            try
            {
                lock (Controls)
                {
                    if (!Controls.ContainsKey(control))
                    {
                        Controls.Add(control, new InternalEvents());

                        if (Initialized && Connected)
                        {
                            AddControlToChangeGroup addControl = new AddControlToChangeGroup();
                            addControl.ControlParams = new AddControlToChangeGroupParams();
                            addControl.ControlParams.Controls.Add(control.Name);

                            commandQueue.Enqueue(JsonConvert.SerializeObject(addControl));

                            if (Debug)
                                CrestronConsole.PrintLine("QsysCore RegisterControl {0} registered", control.Name);
                        }
                    }
                }

                return true;
            }
            catch (Exception e)
            {
                CrestronConsole.PrintLine("QsysCore RegisterControl exception: " + e.Message);
                return false;
            }
        }
    }
}

